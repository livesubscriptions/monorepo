export const withWrapper = <T>(
  asyncIterator: AsyncIterator<T>,
  onPre?: () => void,
  onPost?: (value: IteratorResult<T>) => void,
): AsyncIterator<T> => {
  const next = asyncIterator.next;

  asyncIterator.next = async () => {
    if (onPre) {
      onPre();
    }

    let result = await next.call(asyncIterator);

    if (onPost) {
      onPost(result);
    }

    return result;
  };

  return asyncIterator;
};
