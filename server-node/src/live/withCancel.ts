export const withCancel = <T>(asyncIterator: AsyncIterator<T>, onCancel: Function): AsyncIterator<T> => {
  const asyncReturn = asyncIterator.return;

  asyncIterator.return = () => {
    onCancel();
    return asyncReturn ? asyncReturn.call(asyncIterator) : Promise.resolve({ value: undefined, done: true });
  };

  return asyncIterator;
};
