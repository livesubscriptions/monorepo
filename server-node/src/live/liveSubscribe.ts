import { SubscribeFunction } from 'subscriptions-transport-ws/dist/server';
import { LiveData, LivePatchData } from '../.mono/model/model';
import { InMemLiveState, LiveState } from '../.mono/model/live-state';
import { create } from 'jsondiffpatch';

const { compare } = require('fast-json-patch');

let _subscribeFn: SubscribeFunction;
export let _liveState: LiveState;
export let _liveKeyword: string;
export let _idFieldsByTypename: Record<string, string>;
export let _objectIdentifier: ((value: any, index: number) => string | undefined) | undefined;

export interface LiveSubscribeBuilderConfig {
  liveState?: LiveState;
  liveKeyword?: string;
  idFieldsByTypename?: Record<string, string>;
  objectIdentifier?: (value: any, index: number) => string | undefined;
}

export const liveSubscribeBuilder = (subscribeFn: SubscribeFunction, config: LiveSubscribeBuilderConfig = {}) => {
  _subscribeFn = subscribeFn;
  _liveState = config.liveState ?? new InMemLiveState();
  _liveKeyword = config.liveKeyword ?? 'live';
  _idFieldsByTypename = config.idFieldsByTypename ?? {};
  _objectIdentifier = config.objectIdentifier;
  return liveSubscribe;
};

const diffpatcher = create({
  objectHash: (value: any, index: number) => {
    let id: string | undefined = undefined;
    if (_objectIdentifier) {
      id = _objectIdentifier(value, index);
    }
    if (!id && value?.__typename) {
      const field = _idFieldsByTypename[value.__typename];
      if (field) {
        id = value[field];
      }
    }

    return id ?? `index-${index}`;
  },
});

export function minifyLiveData(liveData: LiveData, liveState: LiveState = _liveState) {
  let result: LivePatchData = { liveId: liveData.liveId };

  const previousRecord = liveState.get<LivePatchData>(liveData.liveId);
  if (previousRecord) {
    const count = 1 + (previousRecord.count ?? 0);
    delete previousRecord.count;

    const patch = JSON.stringify(compare(previousRecord, liveData));
    const diff = JSON.stringify(diffpatcher.diff(previousRecord, liveData));

    if (diff && diff.length < patch.length) {
      result.diff = diff;
    } else {
      result.patch = patch;
    }

    result.count = count;
    liveData.count = count;
  } else {
    result = { ...liveData };
  }

  liveState.set(liveData?.liveId, liveData);
  return result;
}

const liveSubscribe: SubscribeFunction = async (
  schema,
  document,
  rootValue,
  contextValue,
  variableValues?,
  operationName?,
  fieldResolver?,
  subscribeFieldResolver?,
) => {
  const result = await _subscribeFn(
    schema,
    document,
    rootValue,
    contextValue,
    variableValues,
    operationName,
    fieldResolver,
    subscribeFieldResolver,
  );
  const resultIter = result as AsyncIterableIterator<any>;

  return {
    ...resultIter,
    next: async () => {
      const next: any = await resultIter.next();
      const liveKeys = Object.keys(next?.value?.data ?? {}).filter(key => key.startsWith(_liveKeyword));
      for (let i = 0; i < liveKeys.length; i++) {
        const liveKey = liveKeys[i];
        try {
          const newRecord = next.value.data[liveKey] as LivePatchData;
          if (newRecord?.liveId) {
            next.value.data[liveKey] = minifyLiveData(newRecord);
          }
        } catch (e) {
          console.log(`liveSubscribe error`, e);
        }
      }

      return next;
    },
  };
};
