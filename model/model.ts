export interface LiveData {
  liveId: string;
  [key: string]: any;
}

export interface LivePatchData extends LiveData {
  patch?: string;
  diff?: string;
  count?: number;
}

export const LIVEPATCH_TAG = 'LIVEPATCH';
export const EXISTS_TAG = 'EXISTS';
export const COUNT_TAG = 'COUNT_TAG';
export const NO_STATE_TAG = 'NO_STATE_TAG';
