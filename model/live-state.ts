export interface LiveState {
  set<T = unknown>(key: string, value: T): unknown;

  get<T = unknown>(key: string): T;

  delete(key: string): unknown;
}

export class InMemLiveState implements LiveState {
  clientState: Record<any, any> = {};

  get<T = unknown>(key: string) {
    return this.clientState[key];
  }

  delete(key: string) {
    delete this.clientState[key];
  }

  set<T = unknown>(key: string, value: T) {
    this.clientState[key] = value;
  }
}
