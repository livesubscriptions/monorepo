import { RequestHandler } from '@apollo/client/link/core/types';
import { Observable } from '@apollo/client';
import { patch } from 'jsondiffpatch';
import { applyPatch } from 'fast-json-patch';
import { COUNT_TAG, LiveData, LIVEPATCH_TAG, LivePatchData, NO_STATE_TAG } from '../../.mono/model/model';
import { InMemLiveState, LiveState } from '../../.mono/model/live-state';

export let _liveState: LiveState;
export let _liveKeyword: string;

export const liveRequestHandlerBuilder = (
  liveState: LiveState = new InMemLiveState(),
  liveKeyword: string = 'live',
) => {
  _liveState = liveState;
  _liveKeyword = liveKeyword;
  return liveRequestHandler;
};

const reconstructContent = (liveId: string, liveData: LiveData) => {
  const liveRecord = _liveState.get<LivePatchData>(liveId);
  if (!liveRecord) {
    throw Error(
      `${LIVEPATCH_TAG} - ${NO_STATE_TAG} - Received patch for liveId ${liveId}, but not state present locally.`,
    );
  }

  const currentCount = liveRecord.count ?? 0;
  const newCount = liveData.count;

  if (currentCount + 1 !== newCount) {
    throw Error(
      `${LIVEPATCH_TAG} - ${COUNT_TAG} - Missing patch count for liveId ${liveId}, locally at ${currentCount}, received ${newCount}`,
    );
  }

  let record: any;
  if (liveData.diff) {
    record = patch(liveRecord, JSON.parse(liveData.diff));
  } else if (liveData.patch) {
    record = applyPatch(liveRecord, JSON.parse(liveData.patch), undefined, false).newDocument;
  }
  record.count = newCount;
  return { record, newCount };
};

const processData = (data: any): { data: any; liveIds: string[] } => {
  const liveIds: string[] = [];
  const liveKeys = Object.keys(data?.data ?? {}).filter(key => key.startsWith(_liveKeyword));
  liveKeys.forEach(liveKey => {
    const liveData = data?.data[liveKey] as LiveData;
    const liveId = liveData?.liveId;

    if (liveId) {
      liveIds.push(liveId);

      if (liveData.patch || liveData.diff) {
        const { record, newCount } = reconstructContent(liveId, liveData);

        _liveState.set(liveId, record);
        data.data[liveKey] = { ...record, liveId: `${newCount}-${liveId}` };
      } else {
        _liveState.set(liveId, liveData);
        data.data[liveKey] = { ...data.data[liveKey], liveId: `0-${liveId}` };
      }
    }
  });

  return { data, liveIds };
};

const clearLiveIds = (liveIds: string[] = []) => {
  liveIds.forEach(item => _liveState.delete(item));
};

export const liveRequestHandler: RequestHandler = (operation, forward) => {
  return new Observable(observer => {
    let liveIds: string[] = [];
    const subscription = forward(operation).subscribe(
      value => {
        const processedData = processData(value);
        liveIds = processedData.liveIds;
        observer.next(processedData.data);
      },
      error => {
        clearLiveIds(liveIds);
        observer.error(error);
      },
      () => {
        clearLiveIds(liveIds);
        observer.complete();
      },
    );
    return () => {
      clearLiveIds(liveIds);
      subscription.unsubscribe();
    };
  });
};
