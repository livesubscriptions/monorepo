import { gql } from 'apollo-server-express';

export const ServiceTypeDefs = gql`
  type User {
    name: String
  }
  type Query {
    getAllUsers: [User]
  }
  type Subscription {
    liveExampleListData: ExampleListData
  }
  type ExampleListData {
    liveId: String!
    exampleData: ExampleData
    listData: [ExampleData]
  }
  type ExampleData {
    someNumber: String
    array: [String!]
    nestedArray: [NestedData]
  }
  type NestedData {
    someText: String
    anotherArray: [Keyed!]
  }
  type Keyed {
    keyA: String
    keyB: String
  }
`;
