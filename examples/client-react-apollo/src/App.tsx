import React, { useState } from 'react';
import './App.css';
import { LiveDataPage } from './componets/LiveDataPage';
import { Button } from 'antd';

function App() {
  const [mounted, setMounted] = useState(true);
  return (
    <>
      <Button onClick={() => setMounted(!mounted)}>{mounted ? 'Unmount' : 'Mount'}</Button>
      {mounted && <LiveDataPage />}
    </>
  );
}

export default App;
